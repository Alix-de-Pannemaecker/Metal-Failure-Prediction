function varargout = MAGIC_BOX(varargin)

% MAGIC_BOX MATLAB code for MAGIC_BOX.fig
%      MAGIC_BOX, by itself, creates a new MAGIC_BOX or raises the existing
%      singleton*.
%
%      H = MAGIC_BOX returns the handle to a new MAGIC_BOX or the handle to
%      the existing singleton*.
%
%      MAGIC_BOX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAGIC_BOX.M with the given input arguments.
%
%      MAGIC_BOX('Property','Value',...) creates a new MAGIC_BOX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MAGIC_BOX_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MAGIC_BOX_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MAGIC_BOX

% Last Modified by GUIDE v2.5 12-Jan-2014 13:34:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MAGIC_BOX_OpeningFcn, ...
                   'gui_OutputFcn',  @MAGIC_BOX_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MAGIC_BOX is made visible.
function MAGIC_BOX_OpeningFcn(hObject, eventdata, handles, varargin)

% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MAGIC_BOX (see VARARGIN)

% Choose default command line output for MAGIC_BOX
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MAGIC_BOX wait for user response (see UIRESUME)
% uiwait(handles.figure1);
axes(handles.axes1);
xlabel('Time (s)');
ylabel('Load (N/mm)');

axes(handles.axes3);
xlabel('Time (s)');
ylabel('Load (MPa)');

% mise � 0 des valeurs
set(handles.Longueur_plan,'string','0');
set(handles.Epaisseur_plan,'string','0');
set(handles.Nom_materiau_plan,'string','0');
set(handles.Young_plan,'string','0');
set(handles.Poisson_plan,'string','0');

set(handles.Rayon,'string','0');
set(handles.Nom_materiau_cylindre,'string','0');
set(handles.Young_cylindre,'string','0');
set(handles.Poisson_cylindre,'string','0');

set(handles.Nom_modele,'string','0');
set(handles.Force_normale,'string','0');
set(handles.Frottement_contact,'string','0');
set(handles.Increment_step,'string','0');
set(handles.Temps_step,'string','0');

set(handles.Demi_largeur,'string','0');
set(handles.Frottement_fissure,'string','0');
set(handles.L1,'string','0');
set(handles.Angle1,'string','0');
set(handles.L2,'string','0');
set(handles.Angle2,'string','0');
set(handles.L3,'string','0');
set(handles.Angle3,'string','0');

set(handles.Sigma_max_fretting,'string','0');
set(handles.R_fretting,'string','0');
set(handles.Decalage_fretting,'string','0');
set(handles.Frequence_fretting,'string','0');

set(handles.Sig_ext_MPa,'string','0');

% gestion des boutons radios
set(handles.Fretting_simple,'value',1);
set(handles.Fretting_precontraint,'value',0);

% masque des zones
set(handles.Sig_ext_MPa,'Enable','off');

% --- Outputs from this function are returned to the command line.
function varargout = MAGIC_BOX_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Fretting_simple.
function Fretting_simple_Callback(hObject, eventdata, handles)
% hObject    handle to Fretting_simple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Fretting_simple
set(handles.Fretting_simple,'value',1);
set(handles.Fretting_precontraint,'value',0);

set(handles.Sig_ext_MPa,'Enable','off');

hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');


% --- Executes on button press in Fretting_precontraint.
function Fretting_precontraint_Callback(hObject, eventdata, handles)
% hObject    handle to Fretting_precontraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Fretting_precontraint

set(handles.Fretting_simple,'value',0);
set(handles.Fretting_precontraint,'value',1);

set(handles.Sig_ext_MPa,'Enable','on');

hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% R�cup�ration de la courbe de fretting precontraint
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');



function Nom_modele_Callback(hObject, eventdata, handles)
% hObject    handle to Nom_modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nom_modele as text
%        str2double(get(hObject,'String')) returns contents of Nom_modele as a double


% --- Executes during object creation, after setting all properties.
function Nom_modele_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nom_modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rayon_Callback(hObject, eventdata, handles)
% hObject    handle to Rayon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rayon as text
%        str2double(get(hObject,'String')) returns contents of Rayon as a double


% --- Executes during object creation, after setting all properties.
function Rayon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rayon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Longueur_plan_Callback(hObject, eventdata, handles)
% hObject    handle to Longueur_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Longueur_plan as text
%        str2double(get(hObject,'String')) returns contents of Longueur_plan as a double


% --- Executes during object creation, after setting all properties.
function Longueur_plan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Longueur_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Epaisseur_plan_Callback(hObject, eventdata, handles)
% hObject    handle to Epaisseur_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Epaisseur_plan as text
%        str2double(get(hObject,'String')) returns contents of Epaisseur_plan as a double


% --- Executes during object creation, after setting all properties.
function Epaisseur_plan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Epaisseur_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Demi_largeur_Callback(hObject, eventdata, handles)
% hObject    handle to Demi_largeur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Demi_largeur as text
%        str2double(get(hObject,'String')) returns contents of Demi_largeur as a double


% --- Executes during object creation, after setting all properties.
function Demi_largeur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Demi_largeur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Frottement_contact_Callback(hObject, eventdata, handles)
% hObject    handle to Frottement_contact (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frottement_contact as text
%        str2double(get(hObject,'String')) returns contents of Frottement_contact as a double


% --- Executes during object creation, after setting all properties.
function Frottement_contact_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frottement_contact (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Frottement_fissure_Callback(hObject, eventdata, handles)
% hObject    handle to Frottement_fissure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frottement_fissure as text
%        str2double(get(hObject,'String')) returns contents of Frottement_fissure as a double


% --- Executes during object creation, after setting all properties.
function Frottement_fissure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frottement_fissure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Force_normale_Callback(hObject, eventdata, handles)
% hObject    handle to Force_normale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Force_normale as text
%        str2double(get(hObject,'String')) returns contents of Force_normale as a double


% --- Executes during object creation, after setting all properties.
function Force_normale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Force_normale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Nom_materiau_cylindre_Callback(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nom_materiau_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Nom_materiau_cylindre as a double


% --- Executes during object creation, after setting all properties.
function Nom_materiau_cylindre_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Young_cylindre_Callback(hObject, eventdata, handles)
% hObject    handle to Young_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Young_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Young_cylindre as a double


% --- Executes during object creation, after setting all properties.
function Young_cylindre_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Young_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Poisson_cylindre_Callback(hObject, eventdata, handles)
% hObject    handle to Poisson_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Poisson_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Poisson_cylindre as a double


% --- Executes during object creation, after setting all properties.
function Poisson_cylindre_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Poisson_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Nom_materiau_plan_Callback(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nom_materiau_plan as text
%        str2double(get(hObject,'String')) returns contents of Nom_materiau_plan as a double


% --- Executes during object creation, after setting all properties.
function Nom_materiau_plan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Young_plan_Callback(hObject, eventdata, handles)
% hObject    handle to Young_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Young_plan as text
%        str2double(get(hObject,'String')) returns contents of Young_plan as a double


% --- Executes during object creation, after setting all properties.
function Young_plan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Young_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Poisson_plan_Callback(hObject, eventdata, handles)
% hObject    handle to Poisson_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Poisson_plan as text
%        str2double(get(hObject,'String')) returns contents of Poisson_plan as a double


% --- Executes during object creation, after setting all properties.
function Poisson_plan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Poisson_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Increment_step_Callback(hObject, eventdata, handles)
% hObject    handle to Increment_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Increment_step as text
%        str2double(get(hObject,'String')) returns contents of Increment_step as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end


% --- Executes during object creation, after setting all properties.
function Increment_step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Increment_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Temps_step_Callback(hObject, eventdata, handles)
% hObject    handle to Temps_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Temps_step as text
%        str2double(get(hObject,'String')) returns contents of Temps_step as a double

hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

end


% --- Executes during object creation, after setting all properties.
function Temps_step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Temps_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Sigma_max_fretting_Callback(hObject, eventdata, handles)
% hObject    handle to Sigma_max_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sigma_max_fretting as text
%        str2double(get(hObject,'String')) returns contents of Sigma_max_fretting as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end



% --- Executes during object creation, after setting all properties.
function Sigma_max_fretting_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sigma_max_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Frequence_fretting_Callback(hObject, eventdata, handles)
% hObject    handle to Frequence_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frequence_fretting as text
%        str2double(get(hObject,'String')) returns contents of Frequence_fretting as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end

% --- Executes during object creation, after setting all properties.
function Frequence_fretting_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frequence_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Decalage_fretting_Callback(hObject, eventdata, handles)
% hObject    handle to Decalage_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Decalage_fretting as text
%        str2double(get(hObject,'String')) returns contents of Decalage_fretting as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end


% --- Executes during object creation, after setting all properties.
function Decalage_fretting_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Decalage_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Sig_ext_MPa_Callback(hObject, eventdata, handles)
% hObject    handle to Sig_ext_MPa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sig_ext_MPa as text
%        str2double(get(hObject,'String')) returns contents of Sig_ext_MPa as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end


% --- Executes during object creation, after setting all properties.
function Sig_ext_MPa_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sig_ext_MPa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_fretting_Callback(hObject, eventdata, handles)
% hObject    handle to R_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of R_fretting as text
%        str2double(get(hObject,'String')) returns contents of R_fretting as a double
hold off

% R�cup�ration de l'axe des X
Incr=str2num(get(handles.Increment_step,'string'));
Tmp=str2num(get(handles.Temps_step,'string'));
x=0:Incr:Tmp;

% R�cup�ration de la courbe de fretting
Frequence_fretting=str2num(get(handles.Frequence_fretting,'string'));
Decalage_fretting=str2num(get(handles.Decalage_fretting,'string'));
Sigma_max_fretting=str2num(get(handles.Sigma_max_fretting,'string'));
Rapport_fretting=str2num(get(handles.R_fretting,'string'));

Sigma_min_fretting=Rapport_fretting*Sigma_max_fretting;
A0_fretting=(Sigma_max_fretting+Sigma_min_fretting)/(Sigma_max_fretting-Sigma_min_fretting);
CF1_fretting=(Sigma_max_fretting-Sigma_min_fretting)/2;
y_fretting=(A0_fretting+sin(Frequence_fretting*(x-Decalage_fretting)))*CF1_fretting;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);
legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% R�cup�ration de la courbe de fretting pr�contraint
if (get(handles.Fretting_precontraint,'value')==1)
    
Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));
y_pre=Sig_ext_MPa;

% Trac� de la courbe de fretting
axes(handles.axes1);
plot(x,y_fretting,'--bs', 'LineWidth',2, 'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10);

legend('Load of Simple Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (N/mm)');

% Trac� de la courbe de fretting pr�contraint
axes(handles.axes3);

plot(x,y_pre,'--rs', 'LineWidth',2, 'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);

legend('Load of Prestressed Fretting');
legend('show')
xlabel('Time (s)');
ylabel('Load (MPa)');

end



% --- Executes during object creation, after setting all properties.
function R_fretting_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_fretting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

L=str2num(get(handles.Longueur_plan,'string'));
W=str2num(get(handles.Epaisseur_plan,'string'));
Nom_materiau_Plan=get(handles.Nom_materiau_plan,'string');
E_Plan=str2num(get(handles.Young_plan,'string'));
nu_Plan=str2num(get(handles.Poisson_plan,'string'));

R=str2num(get(handles.Rayon,'string'));
Nom_materiau_Cylindre=get(handles.Nom_materiau_cylindre,'string');
E_Cylindre=str2num(get(handles.Young_cylindre,'string'));
nu_Cylindre=str2num(get(handles.Poisson_cylindre,'string'));

Nom=get(handles.Nom_modele,'string');
FN=str2num(get(handles.Force_normale,'string'));
f_Contact=str2num(get(handles.Frottement_contact,'string'));
initialInc=str2num(get(handles.Increment_step,'string'));
timePeriod=str2num(get(handles.Temps_step,'string'));

a=str2num(get(handles.Demi_largeur,'string'));
f_Fissure=str2num(get(handles.Frottement_fissure,'string'));
L_1=str2num(get(handles.L1,'string'));
Theta_1=str2num(get(handles.Angle1,'string'));
L_2=str2num(get(handles.L2,'string'));
Theta_2=str2num(get(handles.Angle2,'string'));
L_3=str2num(get(handles.L3,'string'));
Theta_3=str2num(get(handles.Angle3,'string'));

sig_max_fret=str2num(get(handles.Sigma_max_fretting,'string'));
R_fret=str2num(get(handles.R_fretting,'string'));
starting_time_fret=str2num(get(handles.Decalage_fretting,'string'));
f_fret=str2num(get(handles.Frequence_fretting,'string'));

Sig_ext_MPa=str2num(get(handles.Sig_ext_MPa,'string'));

% Boutons radios

if get(handles.Fretting_simple,'value')==1 && get(handles.Fretting_precontraint,'value')==0
    FRETTING_PRECONTRAINT='NON';
else
    FRETTING_PRECONTRAINT='OUI';
end

addpath ./CODE/

MAIN(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,FRETTING_PRECONTRAINT,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3);
    
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit42_Callback(hObject, eventdata, handles)
% hObject    handle to Force_normale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Force_normale as text
%        str2double(get(hObject,'String')) returns contents of Force_normale as a double


% --- Executes during object creation, after setting all properties.
function edit42_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Force_normale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to Nom_modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nom_modele as text
%        str2double(get(hObject,'String')) returns contents of Nom_modele as a double


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nom_modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit40_Callback(hObject, eventdata, handles)
% hObject    handle to Frottement_contact (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frottement_contact as text
%        str2double(get(hObject,'String')) returns contents of Frottement_contact as a double


% --- Executes during object creation, after setting all properties.
function edit40_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frottement_contact (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to Demi_largeur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Demi_largeur as text
%        str2double(get(hObject,'String')) returns contents of Demi_largeur as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Demi_largeur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit41_Callback(hObject, eventdata, handles)
% hObject    handle to Frottement_fissure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frottement_fissure as text
%        str2double(get(hObject,'String')) returns contents of Frottement_fissure as a double


% --- Executes during object creation, after setting all properties.
function edit41_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frottement_fissure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L1_Callback(hObject, eventdata, handles)
% hObject    handle to L1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L1 as text
%        str2double(get(hObject,'String')) returns contents of L1 as a double


% --- Executes during object creation, after setting all properties.
function L1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L2_Callback(hObject, eventdata, handles)
% hObject    handle to L2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L2 as text
%        str2double(get(hObject,'String')) returns contents of L2 as a double


% --- Executes during object creation, after setting all properties.
function L2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L3_Callback(hObject, eventdata, handles)
% hObject    handle to L3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L3 as text
%        str2double(get(hObject,'String')) returns contents of L3 as a double


% --- Executes during object creation, after setting all properties.
function L3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Angle1_Callback(hObject, eventdata, handles)
% hObject    handle to Angle1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Angle1 as text
%        str2double(get(hObject,'String')) returns contents of Angle1 as a double


% --- Executes during object creation, after setting all properties.
function Angle1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Angle1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Angle2_Callback(hObject, eventdata, handles)
% hObject    handle to Angle2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Angle2 as text
%        str2double(get(hObject,'String')) returns contents of Angle2 as a double


% --- Executes during object creation, after setting all properties.
function Angle2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Angle2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Angle3_Callback(hObject, eventdata, handles)
% hObject    handle to Angle3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Angle3 as text
%        str2double(get(hObject,'String')) returns contents of Angle3 as a double


% --- Executes during object creation, after setting all properties.
function Angle3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Angle3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

open('CODE\Fissure.pdf');


function edit55_Callback(hObject, eventdata, handles)
% hObject    handle to Poisson_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Poisson_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Poisson_cylindre as a double


% --- Executes during object creation, after setting all properties.
function edit55_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Poisson_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit54_Callback(hObject, eventdata, handles)
% hObject    handle to Young_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Young_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Young_cylindre as a double


% --- Executes during object creation, after setting all properties.
function edit54_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Young_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit53_Callback(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nom_materiau_cylindre as text
%        str2double(get(hObject,'String')) returns contents of Nom_materiau_cylindre as a double


% --- Executes during object creation, after setting all properties.
function edit53_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nom_materiau_cylindre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit52_Callback(hObject, eventdata, handles)
% hObject    handle to Rayon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rayon as text
%        str2double(get(hObject,'String')) returns contents of Rayon as a double


% --- Executes during object creation, after setting all properties.
function edit52_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rayon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

open('CODE\Chargement.pdf');


% --- Executes on button press in Fretting_simple.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to Fretting_simple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Fretting_simple




% --- Executes on button press in Fretting_precontraint.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to Fretting_precontraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Fretting_precontraint



% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
