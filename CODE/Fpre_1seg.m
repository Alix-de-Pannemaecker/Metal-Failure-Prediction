%-----------------------------------------------------------------------
% Appelle et modifie le fichier Fpre_1seg.py
%-----------------------------------------------------------------------
function Fpre_1seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)


Nom_python=strcat(Nom,'.py');

%-----------------------------------------------------------------------
%-------------------PARAMETRES GEOMETRIQUES DE LA FISSURE---------------
%-----------------------------------------------------------------------
%Amor�age de la fissure en -a, largeur de fissure � la surface: 2 microns
p1=-a-0.001; %Position (mm)
p2=-a+0.001; %Position (mm)
p=(p1+p2)/2; %position suivant x de la pointe de fissure quand theta=0

Theta_1=pi*Theta_1/180; %(rad)
x_1=sin(Theta_1)*L_1+p; %coordon�e x de la pointe de fissure 
y_1=-cos(Theta_1)*L_1; %coordon�e y de la pointe de fissure 

%-----------------------------------------------------------------------
%    Ouverture des fichiers PYTHON 
%-----------------------------------------------------------------------

% mettre le nom de fichier comme suit :  'filename.py'
%ouverture du fichier.py
fid=fopen('Fpre_1seg.py','r+');

if (fid == -1)
     sprintf('%s','le fichier n existe pas dans le repertoire utilise')
     pause;
     return; % permet d'arreter l'execution de la fonction.
end

%cr�ation et ouverture du fichier_final.py
fid_bis=fopen(Nom_python,'w+');

%-----------------------------------------------------------------------
%   Cr�ation et modification du fichier_final.py
%-----------------------------------------------------------------------

%Nom du mod�le
for i=1:1:20
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom,''''); %ligne 21

for i=1:1:18
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 39

%Modification du cylindre
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 40
txtLu=fgetl(fid);
soustxtLu=txtLu(1:38);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s%d%s\n',R,'), direction=COUNTERCLOCKWISE, point1=(',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 41

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 48
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))'); %ligne 49

for i=1:1:14
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 63

%Parition du cylindre: premier rectangle

Partition_cylindre_1=a+0.5;
Partition_cylindre_2=-a-0.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 64
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 65
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 66

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 68

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 83

%Parition du cylindre: second rectangle

Partition_cylindre_1=a+1.5;
Partition_cylindre_2=-a-1.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 84
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 85
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 86

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 88

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 101

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
%Modifiaction du plan
L=L/2;
fprintf(fid_bis,'%f%s%f%s%f%s\n',-L,', 0.0), point2=(',-L,', ',-W,'))'); %ligne 102
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s%f%s\n',-L,', ',-W,'), point2=(',L,', ',-W,'))'); %ligne 103
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s\n',L,', ',-W,'), point2=(',L,', 0.0))'); %ligne 104

for i=1:1:3
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu); %ligne 105

%CREATION DE LA FISSURE
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s\n',L,', 0.0), point2=(',p2,', 0.0))'); %ligne 106
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',p2,', 0.0), point2=(',x_1,', ',y_1,'))'); %ligne 107
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',x_1,', ',y_1, '), point2=(',p1,', 0.0))'); %ligne 108
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s\n',p1,', 0.0), point2=(',-L,', 0.0))'); %ligne 109

for i=1:1:3
txtLu=fgetl(fid);
end
for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 116

txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 117

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 132

txtLu=fgetl(fid);
soustxtLu=txtLu(1:41); 
fprintf(fid_bis,'%s',soustxtLu);
%Partitions du plan: cercles en pointe de fissure
xR1=x_1-0.001*sin(Theta_1);
yR1=y_1+0.001*cos(Theta_1);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR1,',',yR1,'))'); %ligne 133
xR2=x_1-(0.001*2)*sin(Theta_1);
yR2=y_1+(0.001*2)*cos(Theta_1);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR2,',',yR2,'))'); %ligne 134
xR3=x_1-(0.001*3)*sin(Theta_1);
yR3=y_1+(0.001*3)*cos(Theta_1);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR3,',',yR3,'))'); %ligne 135
xR4=x_1-(0.001*4)*sin(Theta_1);
yR4=y_1+(0.001*4)*cos(Theta_1);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR4,',',yR4,'))'); %ligne 136
xR5=x_1-(0.001*5)*sin(Theta_1);
yR5=y_1+(0.001*5)*cos(Theta_1);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR5,',',yR5,'))'); %ligne 137
xR6=x_1-(0.001*6)*sin(Theta_1);
yR6=y_1+(0.001*6)*cos(Theta_1);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,'), point1=(',xR6,',',yR6,'))'); %ligne 138

for i=1:1:5
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 141

%Partitions du plan: premier rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+0.5;
Partition_Plan_rectangle_2=-a-0.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 142
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 143
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 144

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 135

%Partition du plan: second rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+1.5;
Partition_Plan_rectangle_2=-a-1.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 148
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 149
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 150

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 140

%Partition du plan: troisi�me rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+0.5;
Partition_Plan_rectangle_2=-a-0.5;

fprintf(fid_bis,'%d%s%f%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W,'), point2=(',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 154
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 155
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%f%s\n',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W,'))'); %ligne 156

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 147

%Partition du plan: quatri�me rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+1.5;
Partition_Plan_rectangle_2=-a-1.5;

fprintf(fid_bis,'%d%s%f%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W,'), point2=(',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 160
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 161
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%f%s\n',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W,'))'); %ligne 162

for i=1:1:3
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 164

%Modifications du roulement

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 177

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',-R,', ',-R-W,'), point2=(',R,', ',-R-W,'))'); %ligne 178
txtLu=fgetl(fid);
soustxtLu=txtLu(1:38);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s%d%s\n',-R-W,'), direction=COUNTERCLOCKWISE, point1=(',R,', ',-R-W,'), point2=(',-R,', ',-R-W,'))'); %ligne 179

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 186
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))'); %ligne 187

for i=1:1:14
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 201

%Parition du roulement: premier rectangle

Partition_roulement_1=a+0.5;
Partition_roulement_2=-a-0.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%f%s%d%s%f%s\n',Partition_roulement_2,', ',-W,'), point2=(',Partition_roulement_2,', ',-W+Partition_roulement_2,'))'); %ligne 202
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W+Partition_roulement_2,'))'); %ligne 203
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_1,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W,'))'); %ligne 204

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))');%ligne 206

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 221

%Parition du roulement: second rectangle

Partition_roulement_1=a+1.5;
Partition_roulement_2=-a-1.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W,'), point2=(',Partition_roulement_2,', ',-W+Partition_cylindre_2,'))'); %ligne 222
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W+Partition_roulement_2,'))'); %ligne 223
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_1,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W,'))'); %ligne 224

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))');%ligne 226

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 239

%MATERIAUX
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Cylindre,''')'); %ligne 240
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Cylindre,'''','].Elastic(table=((',E_Cylindre,', ',nu_Cylindre,'),))'); %ligne 241

txtLu=fgetl(fid);
soustxtLu=txtLu(1:61); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Cylindre,'''',', thickness=None)'); %ligne 242
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 246
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Plan,''')'); %ligne 247
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Plan,'''','].Elastic(table=((',E_Plan,', ',nu_Plan,'),))'); %ligne 248
txtLu=fgetl(fid);
soustxtLu=txtLu(1:63); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Plan,'''',', thickness=None)'); %ligne 249

for i=1:1:33
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 282

%EDITION DU STEP DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%s%s%s%s%s%s%s%s%f%s\n',initialInc,', maxInc=',initialInc,', minInc=1e-20, name=','''','Fretting','''', ', previous=','''','Force normale','''', ', timePeriod=',timePeriod,')'); %ligne 283

for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 289

%RP
txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP1=R+20;
fprintf(fid_bis,'%d%s\n',RP1,', 0.0))'); %ligne 290

txtLu=fgetl(fid);
soustxtLu=txtLu(1:43); 
fprintf(fid_bis,'%s',soustxtLu);
RP2_x=-L-20;
RP2_y=-W/2;
fprintf(fid_bis,'%d%s%d%s\n',RP2_x,', ',RP2_y,', 0.0))'); %ligne 291

txtLu=fgetl(fid);
soustxtLu=txtLu(1:43); 
fprintf(fid_bis,'%s',soustxtLu);
RP3_x=L+20;
RP3_y=-W/2;
fprintf(fid_bis,'%d%s%d%s\n',RP3_x,', ',RP3_y,', 0.0))'); %ligne 292

txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP4=-R-W-20;
fprintf(fid_bis,'%d%s\n',RP4,', 0.0))'); %ligne 293

for i=1:1:41
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 334

%COEFFICIENT DE FROTTEMENT CYLINDRE/PLAN
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',f_Contact,', ), ), temperatureDependency=OFF)'); %ligne 335

for i=1:1:20
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 355

%COEFFICIENT DE FROTTEMENT DANS LA FISSURE
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',f_Fissure,', ), ), temperatureDependency=OFF)'); %ligne 356

for i=1:1:34
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 390

%CONTOUR INTEGRAL
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',p,', 0.0, 0.0), (',x_1,', ',y_1,', 0.0)), )'); %ligne 291

%BOUNDARY CONDITIONS
for i=1:1:34
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 325
txtLu=fgetl(fid);
soustxtLu=txtLu(1:52); 
fprintf(fid_bis,'%s',soustxtLu); 

%CALCUL ANALYTIQUE DE L'ALLONGEMENT DE L'EPROUVETTE
eps=Sig_ext_MPa/E_Plan;
DL=L/2*eps; %allongement au milieu de l'�prouvette (mm)
fprintf(fid_bis,'%d%s\n',-DL,', '); %ligne 426
for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 432
txtLu=fgetl(fid);
soustxtLu=txtLu(1:53); 
fprintf(fid_bis,'%s',soustxtLu); 
fprintf(fid_bis,'%d%s\n',-DL,', '); %ligne 433

%LOADS
for i=1:1:19
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 452

%EDITION DE LA SINUSO�DE DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
sig_min_fret=R_fret*sig_max_fret;
A_0_fret=(sig_min_fret+sig_max_fret)/(sig_max_fret-sig_min_fret);
fprintf(fid_bis,'%d%s\n',A_0_fret,', data=((0.0, 1.0), ),'); %ligne 453
txtLu=fgetl(fid);
soustxtLu=txtLu(1:14); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%s%s%s%s%d%s\n',f_fret,', name=','''','fretting','''',', start=', starting_time_fret,', timeSpan=TOTAL)'); %ligne 454

for i=1:1:3
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);%ligne 457
end

%FORCES
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-Sig_ext_MPa*W,', createStepName='); %ligne 458
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 462
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-FN,', createStepName='); %ligne 463
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 467
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',FN,', createStepName='); %ligne 468
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 472
txtLu=fgetl(fid);
soustxtLu=txtLu(1:52); 
fprintf(fid_bis,'%s',soustxtLu);
sigma_a_fret=(sig_max_fret-sig_min_fret)/2;
fprintf(fid_bis,'%d%s\n',sigma_a_fret,', '); %ligne 473
for i=1:1:42
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 515
 
 %MESH
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
mailles=round(R/10);
fprintf(fid_bis,'%d%s\n',mailles,')'); %ligne 516
 for i=1:1:13
     txtLu=fgetl(fid);
     fprintf(fid_bis,'%s\n',txtLu);
 end %ligne 529
txtLu=fgetl(fid);
soustxtLu=txtLu(1:34); 
fprintf(fid_bis,'%s',soustxtLu);
nombre=round(R);
fprintf(fid_bis,'%d%s\n',nombre,', ratio=10.0)'); %ligne 530

  for i=1:1:39
     txtLu=fgetl(fid);
     fprintf(fid_bis,'%s\n',txtLu);
 end %ligne 569
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
if L_1<0.1
   ratio=L_1*100*2;
   number=round(L_1*1000/2);
else
   ratio=L_1*10*2;
   number=round(ratio*10);
end
fprintf(fid_bis,'%d%s%d%s\n',number,', ratio=',ratio,')');%ligne 570

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu); %ligne 585
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36); 
fprintf(fid_bis,'%s',soustxtLu);
Taille_W=W/20;
fprintf(fid_bis,'%d%s\n',Taille_W,')'); %ligne 586

%Ecriture jusqu'� la fin du fichier
txtLu=fgetl(fid);
while txtLu~=-1
     fprintf(fid_bis,'%s\n',txtLu);
     txtLu=fgetl(fid);
end

fclose(fid);
fclose(fid_bis);

end


