'''
-----------------------------------------------------------------------------
Modele Fretting Simple Cylindre/Plan sans fissure
Alix de Pannemaecker - Docorante LTDS/MATEIS - 2012/2015
-----------------------------------------------------------------------------
'''   
   
from abaqus import *
import testUtils
testUtils.setBackwardCompatibility()
from abaqusConstants import *

import part, material, section, assembly, step, interaction
import regionToolset, displayGroupMdbToolset as dgm, mesh, load, job 
   
#----------------------------------------------------------------------------
   
# Creation du modele
   
Mdb()
modelName = 'NEW_CRACKBOX'	#EDITER LE NOM DU MODELE
myModel = mdb.Model(name=modelName)
   
# Create a new viewport in which to display the model
# and the results of the analysis.
   
myViewport = session.Viewport(name=modelName)
myViewport.makeCurrent()
myViewport.maximize()
   
#---------------------------------------------------------------------------
   
# Creation du cylindre
# Creation du sketch et de la part
   
mySketch = myModel.Sketch(name='cylindreProfile',sheetSize=200.0)
mySketch.sketchOptions.setValues(viewStyle=AXISYM)
mySketch.setPrimaryObject(option=STANDALONE)
   
mySketch.Line(point1=(-80.0, 80.0), point2=(80.0, 80.0)) 	#EDITER CYLINDRE
mySketch.ArcByCenterEnds(center=(0.0, 80.0), direction=COUNTERCLOCKWISE, point1=(-80.0, 80.0), point2=(80.0, 80.0))	#EDITER CYLINDRE
   
myCylindre=myModel.Part(name='Cylindre', dimensionality=TWO_D_PLANAR, type=DEFORMABLE_BODY)
myCylindre.BaseShell(sketch=mySketch)
mySketch.unsetPrimaryObject()
del myModel.sketches['cylindreProfile']
   
myViewport.setValues(displayedObject=myCylindre)
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
# Create a set referring to the whole part
myCylindre.Set(faces=faces, name='All_Cylindre')
   
#Partition 1 du cylindre
f = myCylindre.faces
t = myCylindre.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='cylindreProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myCylindre.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-1.5, 0.0), point2=(-1.5, 1.5)) 	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(-1.5, 1.5), point2=(1.5, 1.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(1.5, 1.5), point2=(1.5, 0.0))	#EDITER PARTITION CYLINDRE
   
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
myCylindre.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
#Partition 2 du cylindre
f = myCylindre.faces
t = myCylindre.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='cylindreProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myCylindre.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-2.5, 0.0), point2=(-2.5, 2.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(-2.5, 2.5), point2=(2.5, 2.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(2.5, 2.5), point2=(2.5, 0.0))	#EDITER PARTITION CYLINDRE
   
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
myCylindre.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
# ---------------------------------------------------------------------------
   
# Creation du plan
# Creation du sketch et de la part
   
mySketch = myModel.Sketch(name='planProfile',sheetSize=200.0)
mySketch.sketchOptions.setValues(viewStyle=AXISYM)
mySketch.setPrimaryObject(option=STANDALONE)
   
mySketch.Line(point1=(-100.0, 0.0), point2=(-100.0, -50.0))	#EDITER PLAN
mySketch.Line(point1=(-100.0, -50.0), point2=(100.0, -50.0))	#EDITER PLAN
mySketch.Line(point1=(100.0, -50.0), point2=(100.0, 0.0))	#EDITER PLAN
mySketch.Line(point1=(100.0, 0.0), point2=(-100, 0.0))	#EDITER PLAN
   
myPlan=myModel.Part(name='Plan', dimensionality=TWO_D_PLANAR, type=DEFORMABLE_BODY)
myPlan.BaseShell(sketch=mySketch)
mySketch.unsetPrimaryObject()
del myModel.sketches['planProfile']
   
myViewport.setValues(displayedObject=myPlan)
faces = myPlan.faces.findAt(((0,-10,0),))	#A EDITER: EPAISSEUR DU PLAN
# Create a set referring to the whole part
myPlan.Set(faces=faces, name='All_Plan')
   
#Partitions du plan
f = myPlan.faces
t = myPlan.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='planProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myPlan.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r,r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
#PREMIER RECTANGLE
   
mySketch.Line(point1=(-1.5, 0.0), point2=(-1.5, -1.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-1.5, -1.5), point2=(1.5, -1.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(1.5, -1.5), point2=(1.5, 0.0))	#EDITER PARTITION PLAN
   
#SECOND RECTANGLE
   
mySketch.Line(point1=(-2.5, 0.0), point2=(-2.5, -2.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-2.5, -2.5), point2=(2.5, -2.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(2.5, -2.5), point2=(2.5, 0.0))	#EDITER PARTITION PLAN
   
faces = myPlan.faces.findAt(((0,-10,0),))	#A EDITER: EPAISSEUR DU PLAN
myPlan.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
# ---------------------------------------------------------------------------
   
# Assign material properties
   
import material
import section
   
# Create linear elastic material
   
myModel.Material(name='TA6V')
myModel.materials['TA6V'].Elastic(table=((119500, 0.287),))	#EDITER MATERIAU CYLINDRE
myModel.HomogeneousSolidSection(name='Cylindrique', material='TA6V', thickness=None)
region = myCylindre.sets['All_Cylindre']
# Assign the above section to the part
myCylindre.SectionAssignment(region=region, sectionName='Cylindrique')
   
myModel.Material(name='Alu')
myModel.materials['Alu'].Elastic(table=((79000, 0.305),))	#EDITER MATERIAU PLAN
myModel.HomogeneousSolidSection(name='Rectangulaire', material='Alu', thickness=None)
region = myPlan.sets['All_Plan']
# Assign the above section to the part
myPlan.SectionAssignment(region=region, sectionName='Rectangulaire')
   
#---------------------------------------------------------------------------
   
# ASSEMBLY
   
myAssembly = myModel.rootAssembly
myViewport.setValues(displayedObject=myAssembly)
myAssembly.DatumCsysByDefault(CARTESIAN)
   
myAssembly.Instance(name='Cylindre-1', part=myCylindre)
myCylindreInstance = myAssembly.instances['Cylindre-1']
myAssembly.Instance(name='Plan-1', part=myPlan)
myPlanInstance = myAssembly.instances['Plan-1']
   
#---------------------------------------------------------------------------
   
# STEPS
   
	#EDITER STEPS (ici modele fretting simple)
myModel.StaticStep(name='Contact', previous='Initial')
myModel.StaticStep(initialInc=0.1, maxInc=0.1, name='Force normale', previous='Contact')
myModel.StaticStep(initialInc=0.125, maxInc=0.125, minInc=1e-020, maxNumInc=1000, name='Fretting Simple', previous='Force normale', timePeriod=1.25)#Editer le Step de fretting
   
#---------------------------------------------------------------------------
   
# INTERACTIONS
   
#Creations des points de reference
myModel.rootAssembly.ReferencePoint(point=(0.0, 100.0, 0.0))	#EDITER RP CYLINDRE
myModel.rootAssembly.ReferencePoint(point=(0.0, -70.0, 0.0))	#EDITER RP PLAN
   
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[7], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage plan', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#1c ]', ), )), u1=ON, u2=ON, ur3=ON)
   
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[6], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage cylindre', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#10 ]', ), )), u1=ON, u2=ON, ur3=ON)
   
# Creation des contacts
myModel.ContactProperty('Tangentiel')
myModel.interactionProperties['Tangentiel'].TangentialBehavior(
    dependencies=0, directionality=ISOTROPIC, formulation=LAGRANGE, 
    pressureDependency=OFF, shearStressLimit=None, slipRateDependency=OFF, 
    table=((0.85, ), ), temperatureDependency=OFF)	#EDITER COEF DE FROTTEMENT
myModel.interactionProperties['Tangentiel'].NormalBehavior(
    allowSeparation=ON, constraintEnforcementMethod=DEFAULT, 
    pressureOverclosure=HARD)
   
myModel.SurfaceToSurfaceContactStd(adjustMethod=NONE, 
    clearanceRegion=None, createStepName='Contact', datumAxis=None, 
    enforcement=SURFACE_TO_SURFACE, initialClearance=OMIT, interactionProperty=
    'Tangentiel', master=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#800 ]', ), )), name='Contact', slave=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#800 ]', ), )), sliding=SMALL, surfaceSmoothing=NONE, 
    thickness=ON)
   
# ------------------------------------------------------------------------
   
# Create loads and boundary conditions 
   
# Assign boundary conditions
myModel.DisplacementBC(amplitude=UNSET, createStepName='Initial', 
    distributionType=UNIFORM, fieldName='', localCsys=None, name='Bridage plan'
    , region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[7], )), u1=SET, u2=SET, 
    ur3=SET)
myModel.DisplacementBC(amplitude=UNSET, createStepName='Initial', 
    distributionType=UNIFORM, fieldName='', localCsys=None, name=
    'Bridage cylindre', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[6], )), u1=UNSET, u2=
    UNSET, ur3=SET)
myModel.DisplacementBC(amplitude=UNSET, createStepName='Contact', 
    distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    'Deplacement', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[6], )), u1=0.0, u2=
    -5e-05, ur3=UNSET)
myModel.boundaryConditions['Deplacement'].deactivate(
    'Force normale')
   
#Creation de la sinusoide de fretting	EDITER LA SINUSOIDE
myModel.PeriodicAmplitude(a_0=0.0, data=((0.0, 1.0), ), 
    frequency=6.28, name='fretting', start=0.0, timeSpan=TOTAL) #a_0:"initial amplitude",frequency:[rad/s],start:"starting time" 
   
#Assign load conditions 	#EDITER FORCES
myModel.ConcentratedForce(cf2=-436.5, createStepName=
    'Force normale', distributionType=UNIFORM, field='', localCsys=None, name=
    'Force normale', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[6], )))
myModel.ConcentratedForce(amplitude='fretting', cf1=200.0, 
    createStepName='Fretting Simple', distributionType=UNIFORM, field='', localCsys=None, 
    name='fretting', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[6], )))
   
#---------------------------------------------------------------------------
   
# MESH	EDITER LE MAILLAGE!
   
#HYPOTHESE:PLAIN STRAIN
myModel.rootAssembly.setElementType(elemTypes=(mesh.ElemType(
    elemCode=CPE4R, elemLibrary=STANDARD, secondOrderAccuracy=OFF, 
    hourglassControl=DEFAULT, distortionControl=DEFAULT), mesh.ElemType(
    elemCode=CPE3, elemLibrary=STANDARD)), regions=(
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#7 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#7 ]', ), ), ))
   
#MAILLAGE
myModel.rootAssembly.setMeshControls(elemShape=TRI, regions=
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#3 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#5 ]', ), ))
myModel.rootAssembly.setMeshControls(elemShape=QUAD, 
    regions=
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#4 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#2 ]', ), ), technique=STRUCTURED)
   
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#10 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#1c ]', ), ), size=10.0)
   
myModel.rootAssembly.seedEdgeByBias(end1Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#8 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#2 ]', ), ), end2Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#20 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#20 ]', ), ), number=60, ratio=10.0)
   
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#7 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#c1 ]', ), ), size=0.1)
   
myModel.rootAssembly.seedEdgeByBias(end1Edges=
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#1000 ]', ), )+\
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#400 ]', ), ), end2Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#40 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#2000 ]', ), ), number=20, ratio=10.0)
   
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#b80 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#f00 ]', ), ), size=0.02)
   
myModel.rootAssembly.generateMesh(regions=(
    myModel.rootAssembly.instances['Cylindre-1'], 
    myModel.rootAssembly.instances['Plan-1']))
   
#---------------------------------------------------------------------------
#CORRECTION MAILLAGE DU PLAN
   
#ADAPTATION DU MAILLAGE SUR W
myModel.rootAssembly.deleteMesh(regions=
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    ('[#1 ]', ), ))
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    ('[#14 ]', ), ), size=2.5) #FAIRE BOUCLE
#ADAPTATION DU MAILLAGE SUR L
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    ('[#8 ]', ), ), size=5) #FAIRE BOUCLE
#REGENERATION DU MAILLAGE
myModel.rootAssembly.generateMesh(regions=(
    myModel.rootAssembly.instances['Plan-1'], ))
#---------------------------------------------------------------------------
   
#OUTPUTS
   
# History output sur le point de ref du cylindre (pour tracer les cycles de fretting imposes)
#Creation du set sur le RP
myModel.rootAssembly.Set(name='RP-Fretting', 
    referencePoints=(myModel.rootAssembly.referencePoints[6], ))
#Outputs sur le RP
myModel.HistoryOutputRequest(createStepName='Fretting Simple', name=
    'fretting', rebar=EXCLUDE, region=
    myModel.rootAssembly.sets['RP-Fretting'], sectionPoints=
    DEFAULT, variables=('CF1', 'CF2', 'CF3', 'CM1', 'CM2', 'CM3')) 
   
#---------------------------------------------------------------------------
   
# Create the job 
   
mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
    explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
    memory=90, memoryUnits=PERCENTAGE, model=modelName, modelPrint=OFF, 
    multiprocessingMode=DEFAULT, name='Job-1', nodalOutputPrecision=SINGLE, 
    numCpus=4, numDomains=4, parallelizationMethodExplicit=DOMAIN, queue=None, 
    scratch='', type=ANALYSIS, userSubroutine='', waitHours=0, waitMinutes=0)	#EDITER JOB: nom et "numDomains", depend de l'ordinateur utilise!
   
# ---------------------------------------------------------------------------