%-----------------------------------------------------------------------
% Appelle et modifie le fichier FS_SANS_FISSURE.py
%-----------------------------------------------------------------------

function Fpre_SANS_FISSURE(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)

Nom_python=strcat(Nom,'.py');

%-----------------------------------------------------------------------
%-------------------PARAMETRES GEOMETRIQUES DE LA FISSURE---------------
%-----------------------------------------------------------------------
%Amor�age de la fissure en -a, largeur de fissure � la surface: 2 microns
p1=-a-0.001; %Position (mm)
%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%    Ouverture des fichiers PYTHON 
%-----------------------------------------------------------------------

% mettre le nom de fichier comme suit :  'filename.py'
%ouverture du fichier.py
fid=fopen('Fpre_SANS_FISSURE.py','r+');

if (fid == -1)
     sprintf('%s','le fichier n existe pas dans le repertoire utilise')
     pause;
     return; % permet d'arreter l'execution de la fonction.
end

%cr�ation et ouverture du fichier_final.py
fid_bis=fopen(Nom_python,'w+');

%-----------------------------------------------------------------------
%   Cr�ation et modification du fichier_final.py
%-----------------------------------------------------------------------

%Nom du mod�le
for i=1:1:20
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom,''''); %ligne 21

for i=1:1:18
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 39

%Modification du cylindre
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 40
txtLu=fgetl(fid);
soustxtLu=txtLu(1:38);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s%d%s\n',R,'), direction=COUNTERCLOCKWISE, point1=(',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 41

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 48
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))'); %ligne 49

for i=1:1:14
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 63

%Parition du cylindre: premier rectangle

Partition_cylindre_1=a+0.5;
Partition_cylindre_2=-a-0.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 64
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 65
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 66

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 68

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 83

%Parition du cylindre: second rectangle

Partition_cylindre_1=a+1.5;
Partition_cylindre_2=-a-1.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 84
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 85
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 86

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 88

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 101

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
%Modifiaction du plan
L=L/2;
fprintf(fid_bis,'%f%s%f%s%f%s\n',-L,', 0.0), point2=(',-L,', ',-W,'))'); %ligne 102
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s%f%s\n',-L,', ',-W,'), point2=(',L,', ',-W,'))'); %ligne 103
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s\n',L,', ',-W,'), point2=(',L,', 0.0))'); %ligne 104
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s\n',L,', 0.0), point2=(',-L,', 0.0))'); %ligne 105

for i=1:1:4
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu); %ligne 106

for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 112

txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 113

for i=1:1:16
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 129

%Partitions du plan: premier rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+0.5;
Partition_Plan_rectangle_2=-a-0.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 130
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 131
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 132

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 135

%Partition du plan: second rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+1.5;
Partition_Plan_rectangle_2=-a-1.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 136
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 137
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 138

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 140

%Partition du plan: troisi�me rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+0.5;
Partition_Plan_rectangle_2=-a-0.5;

fprintf(fid_bis,'%d%s%f%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W,'), point2=(',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 142
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 143
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%f%s\n',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W,'))'); %ligne 144

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 147

%Partition du plan: quatri�me rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+1.5;
Partition_Plan_rectangle_2=-a-1.5;

fprintf(fid_bis,'%d%s%f%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W,'), point2=(',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 148
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'))'); %ligne 149
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%f%s\n',Partition_Plan_rectangle_1,', ',-W+Partition_Plan_rectangle_1,'), point2=(',Partition_Plan_rectangle_1,', ',-W,'))'); %ligne 150

for i=1:1:3
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 152

%Modifications du roulement

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 165

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',-R,', ',-R-W,'), point2=(',R,', ',-R-W,'))'); %ligne 166
txtLu=fgetl(fid);
soustxtLu=txtLu(1:38);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s%d%s\n',-R-W,'), direction=COUNTERCLOCKWISE, point1=(',R,', ',-R-W,'), point2=(',-R,', ',-R-W,'))'); %ligne 167

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 174
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))'); %ligne 175

for i=1:1:14
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 189

%Parition du roulement: premier rectangle

Partition_roulement_1=a+0.5;
Partition_roulement_2=-a-0.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%f%s%d%s%f%s\n',Partition_roulement_2,', ',-W,'), point2=(',Partition_roulement_2,', ',-W+Partition_roulement_2,'))'); %ligne 190
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W+Partition_roulement_2,'))'); %ligne 191
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_1,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W,'))'); %ligne 192

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))');%ligne 194

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 209

%Parition du roulement: second rectangle

Partition_roulement_1=a+1.5;
Partition_roulement_2=-a-1.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W,'), point2=(',Partition_roulement_2,', ',-W+Partition_cylindre_2,'))'); %ligne 210
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_2,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W+Partition_roulement_2,'))'); %ligne 211
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_roulement_1,', ',-W+Partition_roulement_2,'), point2=(',Partition_roulement_1,', ',-W,'))'); %ligne 212

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-R-W,',0),))');%ligne 214

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 227

%MATERIAUX
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Cylindre,''')'); %ligne 228
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Cylindre,'''','].Elastic(table=((',E_Cylindre,', ',nu_Cylindre,'),))'); %ligne 229

txtLu=fgetl(fid);
soustxtLu=txtLu(1:61); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Cylindre,'''',', thickness=None)'); %ligne 230
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 234
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Plan,''')'); %ligne 235
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Plan,'''','].Elastic(table=((',E_Plan,', ',nu_Plan,'),))'); %ligne 236
txtLu=fgetl(fid);
soustxtLu=txtLu(1:63); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Plan,'''',', thickness=None)'); %ligne 237

for i=1:1:33
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 270
%EDITION DU STEP DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%s%s%s%s%s%s%s%s%f%s\n',initialInc,', maxInc=',initialInc,', minInc=1e-20, name=','''','Fretting','''', ', previous=','''','Force normale','''', ', timePeriod=',timePeriod,')'); %ligne 271

for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 277

%RP
txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP1=R+20;
fprintf(fid_bis,'%d%s\n',RP1,', 0.0))'); %ligne 278

txtLu=fgetl(fid);
soustxtLu=txtLu(1:43); 
fprintf(fid_bis,'%s',soustxtLu);
RP2_x=-L-20;
RP2_y=-W/2;
fprintf(fid_bis,'%d%s%d%s\n',RP2_x,', ',RP2_y,', 0.0))'); %ligne 279

txtLu=fgetl(fid);
soustxtLu=txtLu(1:43); 
fprintf(fid_bis,'%s',soustxtLu);
RP3_x=L+20;
RP3_y=-W/2;
fprintf(fid_bis,'%d%s%d%s\n',RP3_x,', ',RP3_y,', 0.0))'); %ligne 280

txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP4=-R-W-20;
fprintf(fid_bis,'%d%s\n',RP4,', 0.0))'); %ligne 281

for i=1:1:42
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 323

%COEFFICIENT DE FROTTEMENT CYLINDRE/PLAN
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',f_Contact,', ), ), temperatureDependency=OFF)'); %ligne 324
   
%BOUNDARY CONDITIONS
for i=1:1:60
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 384
txtLu=fgetl(fid);
soustxtLu=txtLu(1:52); 
fprintf(fid_bis,'%s',soustxtLu); 
%CALCUL ANALYTIQUE DE L'ALLONGEMENT DE L'EPROUVETTE
eps=Sig_ext_MPa/E_Plan;
DL=L/2*eps; %allongement au milieu de l'�prouvette (mm)
fprintf(fid_bis,'%d%s\n',-DL,', '); %ligne 385
for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 391
txtLu=fgetl(fid);
soustxtLu=txtLu(1:53); 
fprintf(fid_bis,'%s',soustxtLu); 
fprintf(fid_bis,'%d%s\n',-DL,', '); %ligne 392

%LOADS
for i=1:1:19
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 411

%EDITION DE LA SINUSO�DE DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
sig_min_fret=R_fret*sig_max_fret;
A_0_fret=(sig_min_fret+sig_max_fret)/(sig_max_fret-sig_min_fret);
fprintf(fid_bis,'%d%s\n',A_0_fret,', data=((0.0, 1.0), ),'); %ligne 412
txtLu=fgetl(fid);
soustxtLu=txtLu(1:14); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%s%s%s%s%d%s\n',f_fret,', name=','''','fretting','''',', start=', starting_time_fret,', timeSpan=TOTAL)'); %ligne 413

for i=1:1:3
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);%ligne 416
end

%FORCES
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-Sig_ext_MPa*W,', createStepName='); %ligne 417
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 421
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-FN,', createStepName='); %ligne 422
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 421
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',FN,', createStepName='); %ligne 427
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne
txtLu=fgetl(fid);
soustxtLu=txtLu(1:52); 
fprintf(fid_bis,'%s',soustxtLu);
sigma_a_fret=(sig_max_fret-sig_min_fret)/2;
fprintf(fid_bis,'%d%s\n',sigma_a_fret,', '); %ligne 432
for i=1:1:43
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 475
 
 %MESH
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
mailles=round(R/10);
fprintf(fid_bis,'%d%s\n',mailles,')'); %ligne 476
 for i=1:1:13
     txtLu=fgetl(fid);
     fprintf(fid_bis,'%s\n',txtLu);
 end %ligne 489
txtLu=fgetl(fid);
soustxtLu=txtLu(1:34); 
fprintf(fid_bis,'%s',soustxtLu);
nombre=round(R);
fprintf(fid_bis,'%d%s\n',nombre,', ratio=10.0)'); %ligne 490

for i=1:1:45
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu); %ligne 535
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:29); 
fprintf(fid_bis,'%s',soustxtLu);
Taille_W=W/20;
fprintf(fid_bis,'%d%s\n',Taille_W,')'); %ligne 536

%Ecriture jusqu'� la fin du fichier
txtLu=fgetl(fid);
while txtLu~=-1
     fprintf(fid_bis,'%s\n',txtLu);
     txtLu=fgetl(fid);
end

fclose(fid);
fclose(fid_bis);

end
