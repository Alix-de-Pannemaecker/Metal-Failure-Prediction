'''
-----------------------------------------------------------------------------
Modele Fretting et Fretting precontraint Cylindre/Plan avec fissure
Alix de Pannemaecker - Docorante LTDS/MATEIS - 2012/2015
-----------------------------------------------------------------------------
'''   
   
from abaqus import *
import testUtils
testUtils.setBackwardCompatibility()
from abaqusConstants import *

import part, material, section, assembly, step, interaction
import regionToolset, displayGroupMdbToolset as dgm, mesh, load, job 
   
#----------------------------------------------------------------------------
   
# Creation du modele
   
Mdb()
modelName = 'NEW_CRACKBOX'	#EDITER LE NOM DU MODELE
myModel = mdb.Model(name=modelName)
   
# Create a new viewport in which to display the model
# and the results of the analysis.
   
myViewport = session.Viewport(name=modelName)
myViewport.makeCurrent()
myViewport.maximize()
   
#---------------------------------------------------------------------------
   
# Creation du cylindre
# Creation du sketch et de la part
   
mySketch = myModel.Sketch(name='cylindreProfile',sheetSize=200.0)
mySketch.sketchOptions.setValues(viewStyle=AXISYM)
mySketch.setPrimaryObject(option=STANDALONE)
   
mySketch.Line(point1=(-80.0, 80.0), point2=(80.0, 80.0)) 	#EDITER CYLINDRE
mySketch.ArcByCenterEnds(center=(0.0, 80.0), direction=COUNTERCLOCKWISE, point1=(-80.0, 80.0), point2=(80.0, 80.0))	#EDITER CYLINDRE
   
myCylindre=myModel.Part(name='Cylindre', dimensionality=TWO_D_PLANAR, type=DEFORMABLE_BODY)
myCylindre.BaseShell(sketch=mySketch)
mySketch.unsetPrimaryObject()
del myModel.sketches['cylindreProfile']
   
myViewport.setValues(displayedObject=myCylindre)
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
# Create a set referring to the whole part
myCylindre.Set(faces=faces, name='All_Cylindre')
   
#Partition 1 du cylindre
f = myCylindre.faces
t = myCylindre.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='cylindreProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myCylindre.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-1.5, 0.0), point2=(-1.5, 1.5)) 	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(-1.5, 1.5), point2=(1.5, 1.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(1.5, 1.5), point2=(1.5, 0.0))	#EDITER PARTITION CYLINDRE
   
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
myCylindre.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
#Partition 2 du cylindre
f = myCylindre.faces
t = myCylindre.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='cylindreProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myCylindre.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-2.5, 0.0), point2=(-2.5, 2.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(-2.5, 2.5), point2=(2.5, 2.5))	#EDITER PARTITION CYLINDRE
mySketch.Line(point1=(2.5, 2.5), point2=(2.5, 0.0))	#EDITER PARTITION CYLINDRE
   
faces = myCylindre.faces.findAt(((0,10,0),))	#Attention faire boucle!
myCylindre.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
# ---------------------------------------------------------------------------
   
# Creation du plan
# Creation du sketch et de la part
   
mySketch = myModel.Sketch(name='planProfile',sheetSize=200.0)
mySketch.sketchOptions.setValues(viewStyle=AXISYM)
mySketch.setPrimaryObject(option=STANDALONE)
   
mySketch.Line(point1=(-100.0, 0.0), point2=(-100.0, -50.0))	#EDITER PLAN
mySketch.Line(point1=(-100.0, -50.0), point2=(100.0, -50.0))	#EDITER PLAN
mySketch.Line(point1=(100.0, -50.0), point2=(100.0, 0.0))	#EDITER PLAN
mySketch.Line(point1=(100.0, 0.0), point2=(-100, 0.0))	#EDITER PLAN
   
myPlan=myModel.Part(name='Plan', dimensionality=TWO_D_PLANAR, type=DEFORMABLE_BODY)
myPlan.BaseShell(sketch=mySketch)
mySketch.unsetPrimaryObject()
del myModel.sketches['planProfile']
   
myViewport.setValues(displayedObject=myPlan)
faces = myPlan.faces.findAt(((0,-10,0),))	#A EDITER: EPAISSEUR DU PLAN
# Create a set referring to the whole part
myPlan.Set(faces=faces, name='All_Plan')
   
#Partitions du plan
f = myPlan.faces
t = myPlan.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='planProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myPlan.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r,r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
#PREMIER RECTANGLE
   
mySketch.Line(point1=(-1.5, 0.0), point2=(-1.5, -1.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-1.5, -1.5), point2=(1.5, -1.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(1.5, -1.5), point2=(1.5, 0.0))	#EDITER PARTITION PLAN
   
#SECOND RECTANGLE
   
mySketch.Line(point1=(-2.5, 0.0), point2=(-2.5, -2.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-2.5, -2.5), point2=(2.5, -2.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(2.5, -2.5), point2=(2.5, 0.0))	#EDITER PARTITION PLAN
   
#TROISIEME RECTANGLE
   
mySketch.Line(point1=(-1.5, -50.0), point2=(-1.5, -48.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-1.5, -48.5), point2=(1.5, -48.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(1.5, -48.5), point2=(1.5, -50.0))	#EDITER PARTITION PLAN
   
#QUATRIEME RECTANGLE
   
mySketch.Line(point1=(-2.5, -50.0), point2=(-2.5, -47.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(-2.5, -47.5), point2=(2.5, -47.5))	#EDITER PARTITION PLAN
mySketch.Line(point1=(2.5, -47.5), point2=(2.5, -50.0))	#EDITER PARTITION PLAN
   
faces = myPlan.faces.findAt(((0,-10,0),))	#A EDITER: EPAISSEUR DU PLAN
myPlan.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
# ---------------------------------------------------------------------------
   
# Creation du roulement
# Creation du sketch et de la part
   
mySketch = myModel.Sketch(name='roulementProfile',sheetSize=200.0)
mySketch.sketchOptions.setValues(viewStyle=AXISYM)
mySketch.setPrimaryObject(option=STANDALONE)
   
mySketch.Line(point1=(-80.0, -130), point2=(80.0, -130.0)) 	#EDITER roulement
mySketch.ArcByCenterEnds(center=(0.0, -130.0), direction=COUNTERCLOCKWISE, point1=(80.0, -130.0), point2=(-80.0, -130.0))	#EDITER roulement
   
myRoulement=myModel.Part(name='Roulement', dimensionality=TWO_D_PLANAR, type=DEFORMABLE_BODY)
myRoulement.BaseShell(sketch=mySketch)
mySketch.unsetPrimaryObject()
del myModel.sketches['roulementProfile']
   
myViewport.setValues(displayedObject=myRoulement)
faces = myRoulement.faces.findAt(((0,-60,0),))	#Attention faire boucle!
#Create a set referring to the whole part
myRoulement.Set(faces=faces, name='All_Roulement')
   
#Partition 1 du roulement
f = myRoulement.faces
t = myRoulement.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='roulementProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myRoulement.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-1.5, -50), point2=(-1.5, -51.5)) 	#EDITER PARTITION ROULEMENT
mySketch.Line(point1=(-1.5, -51.5), point2=(1.5, -51.5))	#EDITER PARTITION ROULEMENT
mySketch.Line(point1=(1.5, -51.5), point2=(1.5, -50.0))	#EDITER PARTITION ROULEMENT
   
faces = myRoulement.faces.findAt(((0,-60,0),))	#Attention faire boucle!
myRoulement.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
#Partition 2 du ROULEMENT
f = myRoulement.faces
t = myRoulement.MakeSketchTransform(sketchPlane=f[0],sketchPlaneSide=SIDE1, origin=(0, 0.0, 0.0))
mySketch = myModel.Sketch(name='roulementProfile', sheetSize=200.0, gridSpacing=2.0, transform=t)
mySketch.setPrimaryObject(option=SUPERIMPOSE)
   
myRoulement.projectReferencesOntoSketch(sketch=mySketch, filter=COPLANAR_EDGES)
r, r1 = mySketch.referenceGeometry, mySketch.referenceVertices
   
mySketch.sketchOptions.setValues(gridOrigin=(0.0, 0.0))
   
mySketch.Line(point1=(-2.5, -50.0), point2=(-2.5, -52.5))	#EDITER PARTITION ROULEMENT
mySketch.Line(point1=(-2.5, -52.5), point2=(2.5, -52.5))	#EDITER PARTITION ROULEMENT
mySketch.Line(point1=(2.5, -52.5), point2=(2.5, -50.0))	#EDITER PARTITION ROULEMENT
   
faces = myRoulement.faces.findAt(((0,-60,0),))	#Attention faire boucle!
myRoulement.PartitionFaceBySketch(faces=faces, sketch=mySketch)
mySketch.unsetPrimaryObject()
del mySketch
   
#---------------------------------------------------------------------------
   
# Assign material properties
   
import material
import section
   
# Create linear elastic material
   
myModel.Material(name='TA6V')
myModel.materials['TA6V'].Elastic(table=((119500, 0.287),))	#EDITER MATERIAU CYLINDRE
myModel.HomogeneousSolidSection(name='Cylindrique', material='TA6V', thickness=None)
region = myCylindre.sets['All_Cylindre']
# Assign the above section to the part
myCylindre.SectionAssignment(region=region, sectionName='Cylindrique')
   
myModel.Material(name='Alu')
myModel.materials['Alu'].Elastic(table=((73400, 0.33),))	#EDITER MATERIAU PLAN
myModel.HomogeneousSolidSection(name='Rectangulaire', material='Alu', thickness=None)
region = myPlan.sets['All_Plan']
# Assign the above section to the part
myPlan.SectionAssignment(region=region, sectionName='Rectangulaire')
   
myModel.Material(name='Acier')
myModel.materials['Acier'].Elastic(table=((210000, 0.3),))
myModel.HomogeneousSolidSection(name='roulement', material='Acier', thickness=None)
region = myRoulement.sets['All_Roulement']
# Assign the above section to the part
myRoulement.SectionAssignment(region=region, sectionName='roulement')
#---------------------------------------------------------------------------
   
# ASSEMBLY
   
myAssembly = myModel.rootAssembly
myViewport.setValues(displayedObject=myAssembly)
myAssembly.DatumCsysByDefault(CARTESIAN)
   
myAssembly.Instance(name='Cylindre-1', part=myCylindre)
myCylindreInstance = myAssembly.instances['Cylindre-1']
myAssembly.Instance(name='Plan-1', part=myPlan)
myPlanInstance = myAssembly.instances['Plan-1']
myAssembly.Instance(name='Roulement-1', part=myRoulement)
myPlanInstance = myAssembly.instances['Roulement-1']
   
#---------------------------------------------------------------------------
   
# STEPS
   
	#EDITER STEPS (ici modele fretting fatigue)
myModel.StaticStep(initialInc=0.1, maxInc=0.1, minInc=1e-020, maxNumInc=1000, name='Traction', previous='Initial')
myModel.StaticStep(initialInc=0.1, maxInc=0.1, minInc=1e-020, maxNumInc=1000, name='Contact', previous='Traction')
myModel.StaticStep(initialInc=0.1, maxInc=0.1, minInc=1e-020, maxNumInc=1000, name='Force normale', previous='Contact')
myModel.StaticStep(initialInc=0.125, maxInc=0.125, minInc=1e-020, maxNumInc=1000, name='Fretting', previous='Force normale', timePeriod=1.25)#Editer le Step de fretting
   
#---------------------------------------------------------------------------
   
# INTERACTIONS
   
#Creations des points de reference
myModel.rootAssembly.ReferencePoint(point=(0.0, 100.0, 0.0))	#EDITER RP CYLINDRE 'RP1'
myModel.rootAssembly.ReferencePoint(point=(-120, -25.0, 0.0))	#EDITER RP PLAN 'RP2'
myModel.rootAssembly.ReferencePoint(point=(120, -25.0, 0.0))	#EDITER RP PLAN 'RP3'
myModel.rootAssembly.ReferencePoint(point=(0.0, -150.0, 0.0))	#EDITER RP ROULEMENT 'RP4'
   
#Creations des couplage avec les points de reference

	#Cylindre avec RP1
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage_cylindre', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#10 ]', ), )), u1=ON, u2=ON, ur3=ON)
   
	#Surface gauche du plan avec RP2
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[9], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage_plan_gauche', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#400 ]', ), )), u1=ON, u2=ON, ur3=ON)
   
	#Surface droite du plan avec RP3
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[10], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage_plan-droit', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#10000 ]', ), )), u1=ON, u2=ON, ur3=ON)
   
	#Roulement avec RP4
myModel.Coupling(controlPoint=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[11], )), 
    couplingType=KINEMATIC, influenceRadius=WHOLE_SURFACE, localCsys=None, 
    name='couplage_roulement', surface=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#400 ]', ), )), u1=ON, u2=ON, ur3=ON)
   
#Creation des contacts

	#Contact cylindre/plan
myModel.ContactProperty('Tangentiel')
myModel.interactionProperties['Tangentiel'].TangentialBehavior(
    dependencies=0, directionality=ISOTROPIC, formulation=LAGRANGE, 
    pressureDependency=OFF, shearStressLimit=None, slipRateDependency=OFF, 
    table=((0.9, ), ), temperatureDependency=OFF)	#EDITER COEF DE FROTTEMENT
myModel.interactionProperties['Tangentiel'].NormalBehavior(
    allowSeparation=ON, constraintEnforcementMethod=DEFAULT, 
    pressureOverclosure=HARD)
myModel.SurfaceToSurfaceContactStd(adjustMethod=NONE, 
    clearanceRegion=None, createStepName='Contact', datumAxis=None, 
    enforcement=SURFACE_TO_SURFACE, initialClearance=OMIT, interactionProperty=
    'Tangentiel', master=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#c40 ]', ), )), name='frottement', slave=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#118 ]', ), )), sliding=SMALL, surfaceSmoothing=NONE, thickness=ON)
   
	#Contact roulement/plan
myModel.ContactProperty('glissement')
myModel.interactionProperties['glissement'].TangentialBehavior(
    formulation=FRICTIONLESS)
myModel.interactionProperties['glissement'].NormalBehavior(
    allowSeparation=ON, constraintEnforcementMethod=DEFAULT, 
    pressureOverclosure=HARD)
myModel.SurfaceToSurfaceContactStd(adjustMethod=NONE, 
    clearanceRegion=None, createStepName='Contact', datumAxis=None, 
    enforcement=SURFACE_TO_SURFACE, initialClearance=OMIT, interactionProperty=
    'glissement', master=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#188 ]', ), )), name='glissement', slave=regionToolset.Region(
    side1Edges=myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#e00000 ]', ), )), sliding=SMALL, surfaceSmoothing=NONE, thickness=
    ON)
# ------------------------------------------------------------------------
   
# Create loads and boundary conditions 
   
# Assign boundary conditions
	#Cylindre bridage initial
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Initial', distributionType=UNIFORM, fieldName='', localCsys=None, name=
    'bridage_cylindre', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )), u1=UNSET, 
    u2=UNSET, ur3=SET)
	#Roulement bridage initial
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Initial', distributionType=UNIFORM, fieldName='', localCsys=None, name=
    'bridage_roulement', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[11], )), u1=UNSET, 
    u2=UNSET, ur3=SET)
	#Plan bridage initial
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Initial', distributionType=UNIFORM, fieldName='', localCsys=None, name=
    'bridage_plan_droite', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[10], )), u1=SET, 
    u2=SET, ur3=SET)
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Initial', distributionType=UNIFORM, fieldName='', localCsys=None, name=
    'bridage_plan_gauche', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[9], )), u1=UNSET, 
    u2=SET, ur3=SET)
	#Cylindre deplacement
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Traction', distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=
    None, name='depl_cylindre', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )), u1=-0.484, 
    u2=UNSET, ur3=UNSET)
myModel.boundaryConditions['depl_cylindre'].deactivate('Fretting')
	#Roulement deplacement
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Traction', distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=
    None, name='depl_roulement', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[11], )), u1=-0.484, 
    u2=UNSET, ur3=UNSET)
	#Contact cylindre
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Contact', distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=
    None, name='contact_cylindre', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )), u1=UNSET, 
    u2=-0.1, ur3=UNSET)
myModel.boundaryConditions['contact_cylindre'].deactivate(
    'Force normale')
	#Contact roulement
myModel.DisplacementBC(amplitude=UNSET, createStepName=
    'Contact', distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=
    None, name='Contact_roulement', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[11], )), u1=UNSET, 
    u2=0.1, ur3=UNSET)
myModel.boundaryConditions['Contact_roulement'].deactivate(
    'Force normale')
   
# Creation de la sinusoide de fretting	EDITER LA SINUSOIDE
myModel.PeriodicAmplitude(a_0=0.0, data=((0.0, 1.0), ), 
    frequency=6.28, name='fretting', start=0.0, timeSpan=STEP) #a_0:"initial amplitude",frequency:[rad/s],start:"starting time" 
   
# Assign load conditions 	#EDITER FORCES
	#SIGMA EXT [N/mm]
myModel.ConcentratedForce(cf1=-19950.0, createStepName=
    'Traction', distributionType=UNIFORM, field='', localCsys=None, name=
    'Sig_ext', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[9], )))
	#FORCE NORMALE CYLINDRE [N/mm]
myModel.ConcentratedForce(cf2=-455.0, createStepName=
    'Force normale', distributionType=UNIFORM, field='', localCsys=None, name=
    'FN_cylindre', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )))
	#FORCE NORMALE ROULEMENT [N/mm]
myModel.ConcentratedForce(cf2=455.0, createStepName=
    'Force normale', distributionType=UNIFORM, field='', localCsys=None, name=
    'FN_roulement', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[11], )))
	#FRETTING [N/mm]
myModel.ConcentratedForce(amplitude='fretting', cf1=150.0, 
    createStepName='Fretting', distributionType=UNIFORM, field='', localCsys=
    None, name='fretting', region=regionToolset.Region(referencePoints=(
    myModel.rootAssembly.referencePoints[8], )))
#---------------------------------------------------------------------------
   
# MESH	EDITER LE MAILLAGE!
   
#Hypothese: "plain strain"
myModel.rootAssembly.setElementType(elemTypes=(mesh.ElemType(
    elemCode=CPE4R, elemLibrary=STANDARD, secondOrderAccuracy=OFF, 
    hourglassControl=DEFAULT, distortionControl=DEFAULT), mesh.ElemType(
    elemCode=CPE3, elemLibrary=STANDARD)), regions=(
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#7 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#1f ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].faces.getSequenceFromMask(
    mask=('[#7 ]', ), ), ))
   
#Types d'elements TRI FREE ou QUAD STRUCTURED
myModel.rootAssembly.setMeshControls(elemShape=TRI, regions=
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#3 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#e ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].faces.getSequenceFromMask(
    mask=('[#5 ]', ), ))
myModel.rootAssembly.setMeshControls(elemShape=QUAD, 
    regions=
    myModel.rootAssembly.instances['Cylindre-1'].faces.getSequenceFromMask(
    mask=('[#4 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    mask=('[#11 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].faces.getSequenceFromMask(
    mask=('[#2 ]', ), ), technique=STRUCTURED)
   
#MAILLAGE
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#10 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#10400 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#400 ]', ), ), size=10.0)	#ADAPTER LE MAILLAGE
   
myModel.rootAssembly.seedEdgeByBias(end1Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#8 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#8200 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#200 ]', ), ), end2Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#20 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#20800 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#800 ]', ), ), number=60, ratio=10.0)	#ADAPTER LE MAILLAGE
   
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#7 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#70e0 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#7 ]', ), ), size=0.1)
   
myModel.rootAssembly.seedEdgeByBias(end1Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#400 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#200010 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#80 ]', ), ), end2Edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#40 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#400100 ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#8 ]', ), ), number=20, ratio=10.0)
   
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Cylindre-1'].edges.getSequenceFromMask(
    mask=('[#b80 ]', ), )+\
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    mask=('[#9c000f ]', ), )+\
    myModel.rootAssembly.instances['Roulement-1'].edges.getSequenceFromMask(
    mask=('[#170 ]', ), ), size=0.02)
   
myModel.rootAssembly.generateMesh(regions=(
    myModel.rootAssembly.instances['Cylindre-1'], 
    myModel.rootAssembly.instances['Plan-1'], 
    myModel.rootAssembly.instances['Roulement-1']))
   
#---------------------------------------------------------------------------
#CORRECTION MAILLAGE DU PLAN
   
#ADAPTATION DU MAILLAGE SUR W
myModel.rootAssembly.deleteMesh(regions=
    myModel.rootAssembly.instances['Plan-1'].faces.getSequenceFromMask(
    ('[#4 ]', ), ))
myModel.rootAssembly.seedEdgeBySize(edges=
    myModel.rootAssembly.instances['Plan-1'].edges.getSequenceFromMask(
    ('[#10400 ]', ), ), size=2.5) #FAIRE BOUCLE
#REGENERATION DU MAILLAGE
myModel.rootAssembly.generateMesh(regions=(
    myModel.rootAssembly.instances['Plan-1'], ))
#---------------------------------------------------------------------------
   
#OUTPUTS
   
#Outputs generales
myModel.FieldOutputRequest(createStepName='Traction', name=
    'F-Output-1', variables=PRESELECT)
# History output sur le point de ref du cylindre (pour tracer les cycles de fretting imposes)
#Creation du set sur le RP
myModel.rootAssembly.Set(name='RP-Fretting', 
    referencePoints=(myModel.rootAssembly.referencePoints[8], ))
#Outputs sur le RP
myModel.HistoryOutputRequest(createStepName='Fretting', name=
    'fretting', rebar=EXCLUDE, region=
    myModel.rootAssembly.sets['RP-Fretting'], sectionPoints=
    DEFAULT, variables=('CF1', 'CF2', 'CF3', 'CM1', 'CM2', 'CM3')) 
#---------------------------------------------------------------------------
   
# Create the job 
   
mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
    explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
    memory=90, memoryUnits=PERCENTAGE, model=modelName, modelPrint=OFF, 
    multiprocessingMode=DEFAULT, name='TEST', nodalOutputPrecision=SINGLE, 
    numCpus=4, numDomains=4, parallelizationMethodExplicit=DOMAIN, queue=None, 
    scratch='', type=ANALYSIS, userSubroutine='', waitHours=0, waitMinutes=0)	#EDITER JOB: nom et "numDomains", depend de l'ordinateur utilise!
   
#---------------------------------------------------------------------------