%-----------------------------------------------------------------------
%    COUPLAGE ABAQUS/MATLAB: PROGRAMMATION DE FISSURE SUR UN MODELE 
%   ELASTIQUE EN FRETTING SIMPLE ou FRETTING PRECONTRAINT CYLINDRE/PLAN 
%                  (output: script python Abaqus 6.9)
%                                                                      
%       --- Alix de Pannemaecker - Doctorante au LTDS 2012-2015 ---
%-----------------------------------------------------------------------

function [Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,FRETTING_PRECONTRAINT,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3]= MAIN(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,FRETTING_PRECONTRAINT,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)

%-----------------------------------------------------------------------
%-------------------DEBUT DU CODE---------------------------------------
%-----------------------------------------------------------------------

%Nom_python=strcat(Nom,'.py');

if L_1==0 && L_2==0 && L_3==0

    if FRETTING_PRECONTRAINT=='NON'
        
        FS_SANS_FISSURE(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
        
    elseif FRETTING_PRECONTRAINT=='OUI'
        
        Fpre_SANS_FISSURE(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
        
    end

elseif L_1>0 && L_2==0 && L_3==0

    if FRETTING_PRECONTRAINT=='NON'
        
          FS_1seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    elseif FRETTING_PRECONTRAINT=='OUI'
        
          Fpre_1seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    end

elseif L_1>0 && L_2>0 && L_3==0
    
    if FRETTING_PRECONTRAINT=='NON'
        
          FS_2seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    elseif FRETTING_PRECONTRAINT=='OUI'
        
          Fpre_2seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    end

elseif L_1>0 && L_2>0 && L_3>0
    
    if FRETTING_PRECONTRAINT=='NON'
        
          FS_3seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    elseif FRETTING_PRECONTRAINT=='OUI'
        
          Fpre_3seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,Sig_ext_MPa,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)
          
    end

else
    
    sprintf('ATTENTION! FISSURE MAL DEFINIE!')
    
end

end


