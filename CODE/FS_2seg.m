%-----------------------------------------------------------------------
% Appelle et modifie le fichier FS_SANS_FISSURE.py
%-----------------------------------------------------------------------
function FS_2seg(Nom,R,L,W,a,f_Contact,f_Fissure,FN,Nom_materiau_Cylindre,E_Cylindre,nu_Cylindre,Nom_materiau_Plan,E_Plan,nu_Plan,initialInc,timePeriod,f_fret,starting_time_fret,sig_max_fret,R_fret,L_1,L_2,L_3,Theta_1,Theta_2,Theta_3)


Nom_python=strcat(Nom,'.py');


%-----------------------------------------------------------------------
%-------------------PARAMETRES GEOMETRIQUES DE LA FISSURE---------------
%-----------------------------------------------------------------------
%Amor�age de la fissure en -a, largeur de fissure � la surface: 2 microns
p1=-a-0.001; %Position (mm)
p2=-a+0.001; %Position (mm)
p=(p1+p2)/2; %position suivant x de la pointe de fissure quand theta=0

Theta_1=pi*Theta_1/180; %(rad)
x_1=sin(Theta_1)*L_1+p; 
y_1=-cos(Theta_1)*L_1; 

Theta_2=pi*Theta_2/180; %(rad)
x_2=sin(Theta_2)*L_2+x_1; 
y_2=-cos(Theta_2)*L_2+y_1; 

%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%    Ouverture des fichiers PYTHON 
%-----------------------------------------------------------------------

% mettre le nom de fichier comme suit :  'filename.py'
%ouverture du fichier.py
fid=fopen('FS_2seg.py','r+');
if (fid == -1)
     sprintf('%s','le fichier n existe pas dans le repertoire utilise')
     pause;
     return; % permet d'arreter l'execution de la fonction.
end

%cr�ation et ouverture du fichier_final.py
fid_bis=fopen(Nom_python,'w+');

%-----------------------------------------------------------------------
%   Cr�ation et modification du fichier_final.py
%-----------------------------------------------------------------------

%Nom du mod�le
for i=1:1:20
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom,''''); %ligne 21

for i=1:1:18
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 39

%Modification du cylindre
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 40
txtLu=fgetl(fid);
soustxtLu=txtLu(1:38);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s%d%s\n',R,'), direction=COUNTERCLOCKWISE, point1=(',-R,', ',R,'), point2=(',R,', ',R,'))'); %ligne 41

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 48
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))'); %ligne 49

for i=1:1:14
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 63

%Parition du cylindre: premier rectangle
Partition_cylindre_1=a+0.5;
Partition_cylindre_2=-a-0.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 64
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 65
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 66

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 68

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 83

%Parition du cylindre: second rectangle
Partition_cylindre_1=a+1.5;
Partition_cylindre_2=-a-1.5;

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_2,', 0.0), point2=(',Partition_cylindre_2,', ',Partition_cylindre_1,'))'); %ligne 84
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_cylindre_2,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', ',Partition_cylindre_1,'))'); %ligne 85
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_cylindre_1,', ',Partition_cylindre_1,'), point2=(',Partition_cylindre_1,', 0.0))'); %ligne 86

for i=1:1:2
txtLu=fgetl(fid);
end
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:36);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',R,',0),))');%ligne 88

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 101

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
%Modifiaction du plan
L=L/2;
fprintf(fid_bis,'%f%s%f%s%f%s\n',-L,', 0.0), point2=(',-L,', ',-W,'))'); %ligne 102
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s%f%s\n',-L,', ',-W,'), point2=(',L,', ',-W,'))'); %ligne 103
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%f%s\n',L,', ',-W,'), point2=(',L,', 0.0))'); %ligne 104

for i=1:1:3
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu); %ligne 105

txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s\n',L,', 0.0), point2=(',p2,', 0.0))'); %ligne 106
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',p2,', 0.0), point2=(',x_1+0.0005,', ',y_1,'))'); %ligne 107
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1+0.0005,', ',y_1,'), point2=(',x_2,', ',y_2,'))'); %ligne 108
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point2=(',x_1-0.0005,', ',y_1,'))'); %ligne 109
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',x_1-0.0005,', ',y_1, '), point2=(',p1,', 0.0))'); %ligne 110
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s\n',p1,', 0.0), point2=(',-L,', 0.0))'); %ligne 111

for i=1:1:5
txtLu=fgetl(fid);
end
for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 118

txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 119

for i=1:1:15
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 134
txtLu=fgetl(fid);
soustxtLu=txtLu(1:41); 
fprintf(fid_bis,'%s',soustxtLu);
%Partitions du plan: cercles en pointe de fissure
xR1=x_2-0.001*sin(Theta_2);
yR1=y_2+0.001*cos(Theta_2);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR1,',',yR1,'))'); %ligne 135
xR2=x_2-(0.001*2)*sin(Theta_2);
yR2=y_2+(0.001*2)*cos(Theta_2);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR2,',',yR2,'))'); %ligne 136
xR3=x_2-(0.001*3)*sin(Theta_2);
yR3=y_2+(0.001*3)*cos(Theta_2);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR3,',',yR3,'))'); %ligne 137
xR4=x_2-(0.001*4)*sin(Theta_2);
yR4=y_2+(0.001*4)*cos(Theta_2);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR4,',',yR4,'))'); %ligne 138
xR5=x_2-(0.001*5)*sin(Theta_2);
yR5=y_2+(0.001*5)*cos(Theta_2);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR5,',',yR5,'))'); %ligne 139
xR6=x_2-(0.001*6)*sin(Theta_2);
yR6=y_2+(0.001*6)*cos(Theta_2);
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_2,', ',y_2,'), point1=(',xR6,',',yR6,'))'); %ligne 140

for i=1:1:5
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 143
%Partition du plan: premier rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+0.5;
Partition_Plan_rectangle_2=-a-0.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 144
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 145
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 146

for i=1:1:2
txtLu=fgetl(fid);
end
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 149
%Partition du plan: second rectangle
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);

Partition_Plan_rectangle_1=a+1.5;
Partition_Plan_rectangle_2=-a-1.5;

fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', 0.0), point2=(',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'))'); %ligne 150
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',Partition_Plan_rectangle_2,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'))'); %ligne 151
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s\n',Partition_Plan_rectangle_1,', ',Partition_Plan_rectangle_2,'), point2=(',Partition_Plan_rectangle_1,', 0.0))'); %ligne 152

for i=1:1:3
txtLu=fgetl(fid);
end
fprintf(fid_bis,'%s\n',txtLu);
txtLu=fgetl(fid);
soustxtLu=txtLu(1:32); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-W,',0),))'); %ligne 154

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 167
%MATERIAUX
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Cylindre,''')'); %ligne 168
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Cylindre,'''','].Elastic(table=((',E_Cylindre,', ',nu_Cylindre,'),))'); %ligne 169
txtLu=fgetl(fid);
soustxtLu=txtLu(1:61); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Cylindre,'''',', thickness=None)'); %ligne 170
for i=1:1:4
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 174
txtLu=fgetl(fid);
soustxtLu=txtLu(1:22); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s\n','''',Nom_materiau_Plan,''')'); %ligne 175
txtLu=fgetl(fid);
soustxtLu=txtLu(1:18); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s%d%s%d%s\n','''',Nom_materiau_Plan,'''','].Elastic(table=((',E_Plan,', ',nu_Plan,'),))'); %ligne 176
txtLu=fgetl(fid);
soustxtLu=txtLu(1:63); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%s%s%s%s\n','''',Nom_materiau_Plan,'''',', thickness=None)'); %ligne 177

for i=1:1:24
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 201
%EDITION DU STEP DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%f%s%s%s%s%s%s%s%s%s%f%s\n',initialInc,', maxInc=',initialInc,', minInc=1e-05, name=','''','Fretting Simple','''', ', previous=','''','Force normale','''', ', timePeriod=',timePeriod,')'); %ligne 202

for i=1:1:6
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 208
%RP
txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP1=R+20;
fprintf(fid_bis,'%d%s\n',RP1,', 0.0))'); %ligne 209
txtLu=fgetl(fid);
soustxtLu=txtLu(1:48); 
fprintf(fid_bis,'%s',soustxtLu);
RP2=-W-20;
fprintf(fid_bis,'%d%s\n',RP2,', 0.0))'); %ligne 210

for i=1:1:22
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 232
%CONTOUR INTEGRAL
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s%d%s%d%s%d%s\n',x_1,', ',y_1,', 0.0), (',x_2,', ',y_2,', 0.0)), )'); %ligne 233

for i=1:1:7
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 240
%COEFFICIENT DE FROTTEMENT CYLINDRE/PLAN
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',f_Contact,', ), ), temperatureDependency=OFF)'); %ligne 241
for i=1:1:11
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 252
txtLu=fgetl(fid);
soustxtLu=txtLu(1:12); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',f_Fissure,', ), ), temperatureDependency=OFF)'); %ligne 253

%LOADS
for i=1:1:45
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 298
%EDITION DE LA SINUSO�DE DE FRETTING
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
sig_min_fret=R_fret*sig_max_fret;
A_0_fret=(sig_min_fret+sig_max_fret)/(sig_max_fret-sig_min_fret);
fprintf(fid_bis,'%d%s\n',A_0_fret,', data=((0.0, 1.0), ),'); %ligne 299
txtLu=fgetl(fid);
soustxtLu=txtLu(1:14); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%f%s%s%s%s%s%d%s\n',f_fret,', name=','''','fretting','''',', start=', starting_time_fret,', timeSpan=TOTAL)'); %ligne 300
txtLu=fgetl(fid);
fprintf(fid_bis,'%s\n',txtLu);%ligne 301
%FORCES
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
fprintf(fid_bis,'%d%s\n',-FN,', createStepName='); %ligne 302

for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 304
txtLu=fgetl(fid);
soustxtLu=txtLu(1:52); 
fprintf(fid_bis,'%s',soustxtLu);
sigma_a_fret=(sig_max_fret-sig_min_fret)/2;
fprintf(fid_bis,'%d%s\n',sigma_a_fret,', '); %ligne 306
for i=1:1:35
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu);
end %ligne 341
 
 %MESH
txtLu=fgetl(fid);
soustxtLu=txtLu(1:35); 
fprintf(fid_bis,'%s',soustxtLu);
mailles=round(R/10);
fprintf(fid_bis,'%d%s\n',mailles,')'); %ligne 342
 for i=1:1:9
     txtLu=fgetl(fid);
     fprintf(fid_bis,'%s\n',txtLu);
 end %ligne 351
txtLu=fgetl(fid);
soustxtLu=txtLu(1:37); 
fprintf(fid_bis,'%s',soustxtLu);
nombre=round(R);
fprintf(fid_bis,'%d%s\n',nombre,', ratio=10.0)'); %ligne 352
  for i=1:1:37
     txtLu=fgetl(fid);
     fprintf(fid_bis,'%s\n',txtLu);
 end %ligne 389
txtLu=fgetl(fid);
soustxtLu=txtLu(1:34); 
fprintf(fid_bis,'%s',soustxtLu);
if L_2<0.1
   ratio=L_2*100*2;
   number=round(L_2*1000/2);
else
   ratio=L_2*10*2;
   number=round(ratio*10);
end
fprintf(fid_bis,'%d%s%d%s\n',number,', ratio=',ratio,')');%ligne 390

for i=1:1:13
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu); %ligne 403
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
Taille_W=W/20;
fprintf(fid_bis,'%d%s\n',Taille_W,')'); %ligne 404
for i=1:1:3
    txtLu=fgetl(fid);
    fprintf(fid_bis,'%s\n',txtLu); %ligne 407
end
txtLu=fgetl(fid);
soustxtLu=txtLu(1:30); 
fprintf(fid_bis,'%s',soustxtLu);
Taille_L=2*L/20;
fprintf(fid_bis,'%d%s\n',Taille_L,')'); %ligne 408

%Ecriture jusqu'� la fin du fichier
txtLu=fgetl(fid);
while txtLu~=-1
     fprintf(fid_bis,'%s\n',txtLu);
     txtLu=fgetl(fid);
end

fclose(fid);
fclose(fid_bis);

end


